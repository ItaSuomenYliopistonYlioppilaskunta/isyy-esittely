- ISYY on lyhenne termistä Itä-Suomen yliopiston ylioppilaskunta
- ISYYn jäsenmaksu on lukukausimaksu joka on maksettu yliopistoon läsnäolevaksi ilmoittauduttaessa

## Mihin ISYYn jäsenyys oikeuttaa
- Ylioppilaskunnan jäsenyys on oikeus opiskelijakorttiin ja KELAn ateriatukeen, tällä hetkellä opiskelijakortin saa Frankin, Pivon ja Tuudon kautta
- Voi osallistua kaikkien ISYYn kerhojen toimintaan tai perustaa kerhon
- SYKETTÄ liikuntapalvelut ovat saatavilla jäsenmaksun maksaneille


## Kerhot
Kerhot ovat ylioppilaskunnan rahoittamia yhteisöjä jotka järjestävät toimintaa ylioppilaskunnan jäsenille. 

### Kuopiossa toimivat kerhot
- Hyvät kuvat: Ylioppilaskunnan elokuvakerho joka järjestää elokuvanäytöksiä torstaisin Kino Kuvakukossa. Opiskelijoille elokuvakortti maksaa 25€ ja oikeuttaa kaikkiin kauden näytöksiin.
- Kuopion Intellektuelli Vapunedistämisseura – KIVES: "edistää vappua" eli järjestää tapahtumia ja tuottaa muuta vappuisaa sisältöä
- Itä-Suomen Akateeminen Shakki
- Kuopion Kampuksen RooliPelaajat – KuKaRoPe
- Kuopion ylioppilasteatteri – KYT
- Kuopion ylväs yliopistollinen kampuskyykkä-älymystö – KYYKKÄ
- KYYLÄ: Kuopion kampuksen verkkopelikerho
- Pelikerho Kärmes
- Puijon Suunta: Puijon Suunta on Itä-Suomen yliopiston oma suunnistuskerho
- UEF Muslim Students' Club
- UEF Raamattukerho (UBC)
- ESN KISA (Kuopio International Students’ Association)
- https://www.isyy.fi/vapaa-aika/kerhot/kuopion-kerhot.html

## Liikuntapalvelut
SYKETTÄ liikuntapalvelut on molemmissa kampuskaupungeissa toimiva liikuntapalvelu. Kuopiossa se on järjestetty yhdessä Savonia-ammattikorkeakoulun kanssa. 

Tarjontaan kuuluu mm.
- Ohjattua ryhmäliikuntaa (niin monipuolisesti että vaikea tähän tiivistää - kannattaa katsoa sykettä.fi)
- Avoimia lajivuoroja (esim. sulkapalloa, sählyä, lentopalloa... ja muita)
- Kuntosalipalveluita Studentialla, Snellmanialla sekä rajatusti yhteistyökumppaneiden tiloissa
- Lajikokeiluja 
- Liikuntailtapäivän toimintaa
- Aloittelevan liikkujan vertaisohjausta
- Kuntosaliohjausta, ravitsemusohjausta, fysioterapiaopiskelijoiden palveluita, terveysneuvontaa, mittauksia


## Tapahtumat
- Kaupunkisuunnistus vuoden alussa
- Liikuntailtapäivä
- Itsenäisyyspäivän ja vapun perinteet
- Hymy- ja valitusviikko (eli viikko jolloin voi antaa ISYYn kautta palautetta)
- Hyvinvointiviikko 
- (kerhojen tapahtumat)

## Muuta kivaa
ISYYn toimistoilta on lainattavissa liikuntavälineitä ja keppihevonen.

Ylioppilaskunta tarjoaa jäsenilleen edunvalvontaa opintoihin liittyen, esimerkiksi jos jollain kurssilla jos jokin asia on vinksallaan ja et saa sitä ratkaistua keskustelemalla, saat tukea myös ISYYn kautta.

Ylioppilaskunnan suurista linjoista päättää edustajisto, edustajiston päätökset toteuttaa hallitus ja henkilökunta ylläpitää päivittäistä toimintaa.

Toimintaan voi vaikuttaa äänestämällä edustajistovaaleissa 25.-29.10.2021 tai vaalipäivänä 3.11.2021 (sähköinen järjestelmä).

Edustajistovaaleissa ehdolle voi lähteä 29.9.2021 klo 15:00 mennessä.

ISYY valitsee yliopistolle myös hallinnon opiskelijaedustajat eli hallopedit - he pääsevät osallistumaan yliopiston päätöksentekoon. Hallopedhaku on auki 27.9. asti

## Yhteystiedot

isyy.fi

Ja toki muissakin kanavissa kannattaa seurailla, löytyvät kotisivuilta tai palveluiden hakukentistä nimellä ISYY ja sähköpostitiedote tietääkseni tullee kaikille automaattisesti - sen voi myös tilata kotisivuilta